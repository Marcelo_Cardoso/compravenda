# JackieChain

## Passo a passo para utilizar o JackieChain com o anexo .zip
install plugin metamask
https://metamask.io/

install ganache
https://www.trufflesuite.com/ganache

``` bash
npm install
cd contract
truffle migrate --compile-all --reset --network ganache
npm run dev
```

## Versão node
```
Node v10.15.3
```

## Sequencia desde o inicio - não precisa executar caso utilize o arquivo .zip anexado.

``` bash
sudo apt install software-properties-common
sudo add-apt-repository -y ppa:ethereum/ethereum
sudo apt update
sudo apt install ethereum
```

```
node -v
npm -v
sudo apt install curl
sudo apt install -y nodejs
sudo apt install -y build-essential
npm install -g truffle
truffle version
```

Truffle v5.1.0 (core: 5.1.0)
Solidity v0.5.12 (solc-js)
Node v10.15.3
Web3.js v1.2.2

```
npm install
```

### Se não instalar no npm install usar a baixo
``` bash
npm init
npm install web3
npm install solc
```