pragma solidity >0.4.99 <0.6.0;

import "./Ownable.sol";

contract BuyAndSell is Ownable {
    struct Article {
        uint id;
        address payable seller;
        address buyer;
        string name;
        string description;
        uint256 price;
    }

    mapping(uint => Article) public articles;
    uint articleCounter;
    address seller;
    address buyer;
    string name;
    string description;
    uint256 price;

    event LogSellArticle (
        uint indexed _id,
        address indexed _seller,
        string _name,
        uint256 _price);

    event LogBuyArticle (
        uint indexed _id,
        address indexed _seller,
        address indexed _buyer,
        string _name,
        uint256 _price);

    function kill() public onlyOwner {
        selfdestruct(owner);
    }

    function sellArticle(string memory _name, string memory _description, uint256 _price) public {
        articleCounter++;

        articles[articleCounter] = Article(
            articleCounter,
            msg.sender,
            address(0),
            _name,
            _description,
            _price
        );

        emit LogSellArticle(articleCounter, msg.sender, _name, _price);
    }

    function buyArticle(uint _id) public payable{

        require(articleCounter > 0, "Existe um artigo");
        require(_id > 0 && _id <= articleCounter, "Existe artigo com id");
        // buscar artigo
        Article storage article = articles[_id];
        require(article.buyer == address(0), "Artigo ainda não foi vendido");
        require(article.seller != msg.sender, "Não permitir que o vendedor compre seu próprio artigo");
        require(article.price == msg.value, "Verificar se o valor enviado corresponde ao preço do artigo");
        // informações do comprador
        article.buyer = msg.sender;
        article.seller.transfer(msg.value);

        emit LogBuyArticle(_id, article.seller, article.buyer, article.name, article.price);
    }

    // buscar o número de artigos no contrato
    function getNumberOfArticles() public view returns (uint) {
        return articleCounter;
    }

    // buscar e retornar todos os IDs de artigo disponíveis para venda
    function getArticlesForSale() public view returns (uint[] memory) {
        // verificar se há pelo menos um artigo
        if(articleCounter == 0) {
            return new uint[](0);
        }

        // prepare output arrays
        uint[] memory articleIds = new uint[](articleCounter);

        uint numberOfArticlesForSale = 0;
        // informações sobre os artigos
        for (uint i = 1; i <= articleCounter; i++) {
            // manter apenas o ID do artigo ainda não vendido
            if (articles[i].buyer == address(0)) {
                articleIds[numberOfArticlesForSale] = articles[i].id;
                numberOfArticlesForSale++;
            }
        }

        // copiar a matriz de artigos para a matriz menor para venda
        uint[] memory forSale = new uint[](numberOfArticlesForSale);
        for (uint j = 0; j < numberOfArticlesForSale; j++) {
            forSale[j] = articleIds[j];
        }
        return forSale;
    }
}
