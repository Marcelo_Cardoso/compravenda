pragma solidity >0.4.99 <0.6.0;

contract Ownable {
    address payable owner;

    modifier onlyOwner() {
        require(msg.sender == owner, "Chamar contrato");
        _;
    }

    constructor() public {
        owner = msg.sender;
    }
}